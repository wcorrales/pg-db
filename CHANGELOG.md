0.1.0 2020-11-12
----------------

* Released initial version.


0.2.0 2021-12-25
----------------

* Add mogrify function.


0.2.1 2021-12-28
----------------

* Del mogrify function.
